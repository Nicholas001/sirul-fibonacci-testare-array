package com.nicholas.logic;

import java.util.ArrayList;

public class Operation {

    public void testareSir(int[] sirDeNumere) {

        ArrayList<Integer> sirFibonacci = new ArrayList<>();
        ArrayList<Integer> arrayPtTest = new ArrayList<>();

        int indexStart = 0;
        int indexEnd = 0;
        int indexStartMax = 0;
        int indexEndMax = 0;

        for (int x = 0; x < sirDeNumere.length; x++) {
            arrayPtTest.add(sirDeNumere[x]);
        }

        arrayPtTest.add(sirDeNumere[sirDeNumere.length-1] - 1);

        for (int x = 0; x < arrayPtTest.size() - 2; x++) {
            if (arrayPtTest.get(x) + arrayPtTest.get(x + 1) == arrayPtTest.get(x + 2)) {
                indexEnd = x + 2;
            } else {
                if (indexEndMax - indexStartMax < indexEnd - indexStart) {
                    indexStartMax = indexStart;
                    indexEndMax = indexEnd;
                }
                indexStart = x + 1;
            }
        }

        for (int x = indexStartMax; x <= indexEndMax; x++) {
            sirFibonacci.add(arrayPtTest.get(x));
        }

        System.out.println("Cel mai lung subsir, care indeplineste conditiile pentru a fi un sir Fibonacci este: ");

        System.out.println(sirFibonacci.toString());

    }
}
