package com.nicholas.apprunner;

import com.nicholas.display.Display;
import com.nicholas.logic.Operation;

public class Main {

    public static void main(String[] args) {

        System.out.println("Sirul suspus analizarii este: ");

        int[] sirDeNumere = {1, 3, 5, 8, 13, 21, 34, 35, 39, 2, 4, 6, 10, 16, 26, 42, 68, 110};

        Display.display(sirDeNumere);

        Operation obj = new Operation();

        obj.testareSir(sirDeNumere);

    }

}
